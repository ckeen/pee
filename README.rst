Pee - A password manager...because you have to
==============================================

This is the README file for pee a password store/manager for the command line.

Pee will help you choose a secure password for every website or login you need.
Pee will also suggest a randomly choosen username, with Firstname_Lastname taken from http://deron.meranda.us/data/.
You can annotate the password with additional hints, such as password recovery or other "security" questions.
The passwords are stored in an encrypted password store.
The contents are encrypted using a passphrase.

Pee will protect you against the password storage getting lost.
Pee will not protect you against an attacker that can access all of your computer's RAM.
Pee will not protect you against keyloggers as the passphrase to your store will be logged.

Pee will output the password on the console by default.

If you want to have it available for copy and paste, you should use an external program like xsel.

Dependencies
------------

Pee is written in `CHICKEN Scheme`__, the cryptography parts are provided by the tweetnacl egg.
The BLAKE2s_ code has been taken verbatim from the reference implementations and is included in the pee source code.
Other eggs pee depends on are: srfi-1, srfi-4, getopt-long, matchable and stty.

.. _BLAKE2s: https://github.com/BLAKE2/BLAKE2.git
__ https://www.call-cc.org

Installing
----------

After unpacking the source build it using the CHICKEN Scheme compiler::

  $ chicken-install blob-utils getopt-long matchable stty tweetnacl
  $ csc -s -Jc crypto-helper.scm blake2s-ref.c -C -std=c99
  $ csc pee.scm

Or use the provided .setup file::

  $ chicken-install

Building a statically linked executable
---------------------------------------

Sometimes depending on the globally installed CHICKEN runtime is not desireable.
To build a statically linked version of Pee use the provided static-compilation.sh shell script.
The script will try to fetch all dependencies in the current directory, compiles the necessary units.
If all goes well it will clean up afterwards.
If not you will have to manually do the clean up.
If you have cloned from the git repo, a git clean -f -d will do that for you.

Using Pee
---------

A sample session::

  $ pee -i
  Using database file /home/ckeen/.passdb
  I will ask you twice for the passphrase to encrypt the password store with.
  Enter passphrase: 
  Enter passphrase: 
  Password store /home/ckeen/.passdb initialised.
  $ pee -a t
  Using database file /home/ckeen/.passdb
  Enter passphrase: 
  Username [Karey_Zepeda43]: 
  Mode all chars, Length 25 chars, entropy 161 bits
  )!.#ZBR2.Zwia<.X#[N<-/C~j  Mode alpha-numeric, Length 27 chars, entropy 160 bits
  scBUOVEibGRECh7IwZI7NBQyaAx
  Mode easy-to-read, Length 25 chars, entropy 157 bits
  p&?X^E&BlR~FIfYVs=?%+IqSy
  Mode some-funny-chars, Length 25 chars, entropy 157 bits
  -Gyoka9zev@CPbN6f0!df-&QZ
  Mode some-funny-chars, Length 31 chars, entropy 194 bits
  2zoR4An)wKFNkQnDk4-+mOW-&zP-CzC
  Mode some-funny-chars, Length 36 chars, entropy 226 bits
  fphUdPCi7-d64%^2$8/-Fz1v7a5Q1JUsz|s3
  Comment: my secret site password
  Entry for t added.
  $

Cryptography
------------

The key is derived using the BLAKE2s_ key derivation function.
For symmertric encryption the tweetnacl library is used.

If running on OpenBSD, passwords are generated using OpenBSD's `arc4random()`__ RNG.
If running on linux /dev/urandom will be used as a source of random bytes.
If running on any other OS /dev/random will be used as a source for random bytes.
Passwords are choosen from this set of characters "abcdefhijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=~?/\|+,:.<>{}[]".
There are currently several password modes available that substract a subset of the above character set.

The modes are::

  '(("all chars" . "")
    ("alpha-numeric" . "!@#$%^&*()-=~?/\|+,:.<>{}[]")
    ("easy-to-read" . "l1o0I|!ji")
    ("some-funny-chars" . "|\\[]{}<>~&")))




__ http://www.openbsd.org/cgi-bin/man.cgi/OpenBSD-current/man3/arc4random.3

License
-------

The blake2s Hash function has been licensed under a Creative Commons CC0 1.0 Universal license.
The tweetnacl code is in the public domain.
The tweetnacl egg is BSD licensed.

The rest of the pee code comes with a OpenBSD (ISC like) license.

Copyright (c) 2016 Christian Kellermann <ckeen@pestilenz.org>

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
