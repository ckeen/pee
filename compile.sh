#!/bin/sh
set -e

#to debug change this
DEVNULL=/dev/null

echo "Getting version info from git"
echo -n '(define-constant commit-id "'\
     $(git show -q | grep ^commit| awk '{print $2 }') '")' > hash

echo -n '(define-constant program-version "' $(git tag | sort -nr | head -1) '")' > tag

echo -n "Building static pee program... "
chicken-install

echo "Cleaning up."
rm -f *.so *.o *.import.* pee.c

echo "Build done."
