;; Pee - A password manager for the command line
;;
;; Copyright (c) 2016 Christian Kellermann <ckeen@pestilenz.org>
;;
;; Permission to use, copy, modify, and distribute this software for any
;; purpose with or without fee is hereby granted, provided that the above
;; copyright notice and this permission notice appear in all copies.
;;
;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
;; WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
;; ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
;; ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
;; OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

(include "crypto-helper.scm")

(module pee (main)
(import
        scheme
        (chicken base)
        (chicken condition)
        (chicken bitwise)
        (chicken io)
        (chicken port)
        (chicken random)
        (chicken format)
        (chicken string)
        (chicken file)
        (chicken pretty-print)
        (chicken irregex)
        (chicken time)
        (chicken pathname)
        (chicken process-context)
        (chicken sort)
        (chicken time posix))

(import
        srfi-1
        srfi-4
        srfi-13
        srfi-14
        fmt
        matchable
        tweetnacl
        getopt-long
        stty
        crypto-helper)

(include "program-meta.scm")

(define-constant password-chars "abcdefhijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()-=~?/\|+,:.<>{}[]")

(include "names.scm")

(define password-modes
  '(("all chars" . "")
    ("alpha-numeric" . "!@#$%^&*()-=~?/\|+,:.<>{}[]")
    ("easy-to-read" . "l1o0I|!ji")
    ("some-funny-chars" . "|\\[]{}<>~&")))

(define (entropy-per-char password-chars)
  (inexact->exact (floor (* (/ (log (string-length password-chars)) (log 2)) 100))))

(define wanted-entropy (* 20 8))

(define (chars-for-mode chars mode)
  (char-set->string
   (char-set-difference
    (string->char-set chars)
    (string->char-set mode))))

(define (generate-new-password wanted-entropy mode)
  (define (new-indices count)
    (let ((password-chars (chars-for-mode password-chars mode)))
      (let loop ((len count)
                 (idx '()))
        (clear-line)
        (printf "~a/~a random bytes recieved.~!" (- count len) count)
        (cond ((zero? len) (clear-line) idx)
              (else
               (let ((new (filter (lambda (n)
                                    (< n (string-length password-chars)))
                                  (u8vector->list (random-bytes len)))))
                 (loop (- len (length new)) (append new idx))))))))
  (list->string (map (lambda (i)
                       (string-ref password-chars i))
                     (new-indices (inexact->exact
                                   (round (/ (* 100 wanted-entropy) (entropy-per-char (chars-for-mode password-chars mode)))))))))

(define (prompt-for msg #!optional default)
  (if default (printf "~a [~a]: " msg default)
      (printf "~a: " msg))
  (let ((l (read-line)))
    (if (and default (equal? "" l))
        default
        l)))

(define (ask-for-choice msg . options)
  (with-stty
   '(not echo icanon opost)
   (lambda ()
     (let loop ()
       (clear-line)
       (printf "~a [~a]: " msg (apply string-append options))
       (let ((answer (string (read-char))))
         (cond ((member answer options) =>
                (lambda (c)
                  (clear-line)
                  (car (string->list (car c)))))
               (else (loop))))))))

(define (clear-line)
  (printf "\r~a" (string #\escape #\[ #\K)))

(define (ask-yes-or-no msg)
  (eqv? #\y (ask-for-choice msg "y" "n")))

(define (random-username)
  (let* ((first-random (random-bytes 2))
         (last-random (random-bytes 2))
         (->number (lambda (u8v)
                     (bitwise-ior (u8vector-ref u8v 1)
                                  (arithmetic-shift (u8vector-ref u8v 1) 8))))
         (first-index (->number first-random))
         (last-index (->number last-random))
         (number-first-names (car (alist-ref 'sizes names)))
         (number-last-names (cadr (alist-ref 'sizes names)))
         (first-name (list-ref (alist-ref 'first names)
                               (modulo first-index number-first-names)))
         (last-name (list-ref (alist-ref 'last names)
                              (modulo last-index number-last-names)))
         (number (->string (u8vector-ref (random-bytes 1) 0))))
    (string-titlecase (string-append first-name "_" last-name number))))

(define (new-password)
  (define (ask-for-manual-password)
    (with-stty
     '(not echo)
     (lambda ()
       (printf "Enter new password: ")
       (let ((l (read-line)))
         (print "\r")
         l))))
  (let manual-loop ()
    (if (ask-yes-or-no "Invent your own password?")
        (let ((p1 (ask-for-manual-password))
              (p2 (ask-for-manual-password)))
          (unless (equal? p1 p2) (print "Passwords do not match.") (manual-loop))
          p1)
        (let password-loop ((e wanted-entropy)
                            (modes password-modes))
          (let* ((m (car modes))
                 (p (generate-new-password e (cdr m)))
                 (entropy-delta (cond ((< e 64) 8)
                                      ((< e 128) 16)
                                      (else 32))))
            (printf "Mode ~a, Length ~a chars, entropy ~a bits~%"
                    (car m)
                    (string-length p)
                    (quotient (* (string-length p) (entropy-per-char (chars-for-mode password-chars (cdr m)))) 100))
            (print p)
            (let dialog-loop ()
              (let ((choice (ask-for-choice "Use this password?" "y" "n" "+" "-" " " "m" "?")))
                (case choice
                  ((#\space #\n) (password-loop e modes))
                  ((#\+) (password-loop (+ e entropy-delta) modes))
                  ((#\-) (password-loop (max 32 (- e entropy-delta)) modes))
                  ((#\m) (password-loop e (append (cdr modes) (list m))))
                  ((#\?)
                   (printf "y - accept password~%+ - increase password length~%- - decrease password length~%n/space - new password~%m\t - next password mode~%")
                   (dialog-loop))
                  (else p)))))))))

(define (get-hashed-passphrase)
  (with-stty
   '(not echo)
   (lambda ()
     (display "Enter passphrase: " (current-error-port))
     (let ((l (read-line)))
       (newline (current-error-port))
       (hash-passphrase l)))))

(define (enc/dec-file content passphrase op)
  (let ((sbox (op passphrase))
        (nonce (make-u8vector symmetric-box-noncebytes 0)))
    (sbox content nonce)))

(define (decrypt-file file passphrase)
  (let ((content (with-input-from-file file (lambda () (read-string #f)))))
    (enc/dec-file content passphrase symmetric-unbox)))

(define (check-content content)
  (condition-case
   (with-input-from-string
       (with-output-to-string
         (lambda () (pp content)))
     read)
   (e () (error "Internal error: Writing of unserialisable object detected."))))

(define (encrypt-file file content passphrase)
  (check-content content)
  (let ((cyphertext (enc/dec-file
                         (with-output-to-string (lambda () (pp content)))
                         passphrase
                         symmetric-box)))
    (unless cyphertext
            (print "Error: cannot encrypt password store.")
            (exit 1))
    (with-output-to-file file
      (lambda () (display cyphertext)))))

(define (db-keys alist) (map car alist))

(define (update-db db key #!key user password comment)
  (let ((entry (or (alist-ref key db equal?) (make-list 3 ""))))
    (alist-update key
                  (match-let (((u p c) entry))
                             (list
                              (or user u)
                              (or password p)
                              (or comment c)))
                  db
                  equal?)))

(define (print-entries entries #!key show-password (show-headers #t) (prefixes '()))
  (let ((users (map first entries))
        (passwords (if show-password
                       (map second entries)
                       (make-list (length entries) "***")))
        (comments (map third entries))
        (dates (map (lambda (e)
                      (time->string (seconds->local-time (inexact->exact (fourth e))) "%Y-%m-%d %H:%M:%S"))
                    entries)))
    (fmt #t
         (tabular
          (cat (if show-headers (cat "Label" nl) "") (fmt-join dsp prefixes nl)) " "
          (cat (if show-headers (cat "Username" nl) "") (fmt-join dsp users nl)) " "
          (cat (if show-headers (cat "Passwords" nl) "") (fmt-join dsp passwords nl)) " "
          (cat (if show-headers (cat "Comments" nl) "") (fmt-join dsp comments nl)) " "
          (cat (if show-headers (cat "Last modified" nl) "") (fmt-join dsp dates nl))))))


(define (check-access f)
  (and (file-exists? f)
       (file-readable? f)
       (file-writable? f)))

(define options
  `((init
     "Initialise password store"
     (required #f)
     (value #f)
     (single-char #\i))
    (add
     "Add a new entry to the password store"
     (required #f)
     (value (required ACCOUNT)
            (predicate ,string?))
     (single-char #\a))
    (password
     "Get the password for a given entry"
     (required #f)
     (value (required ACCOUNT)
            (predicate ,string?))
     (single-char #\p))
    (update
     "Change an existing entry in the database"
     (required #f)
     (value (required ACCOUNT)
            (predicate ,string?))
     (single-char #\u))
    (delete
     "Drop an entry from the database"
     (required #f)
     (value (required ACCOUNT)
            (predicate ,string?))
     (single-char #\d))
    (list
     "Shows all info for an entry. Does not show the password."
     (required #f)
     (value (optional ACCOUNT)
            (predicate ,string?))
     (single-char #\l))
    (change-passphrase
     "Reencrypts the store with a new passphrase. Use it regularily."
     (required #f)
     (value #f)
     (single-char #\c))
    (database-file
     "Use FILE as the database"
     (required #f)
     (single-char #\f)
     (value (required FILE)))
    (merge
     "Merge their DB file into our DB file"
     (required #f)
     (single-char #\m)
     (value (required THEIRS)
            (predicate ,check-access)))
    (export
     "Prints the database as an association list (s-expression) on stdout"
     (required #f)
     (value #f))
    (import
     "Reads an association list from FILE, the inverse of export."
     (required #f)
     (value (required FILE)
            (predicate ,check-access)))
    (check-age
     "Checks the age of the passwords to remind you of changing it"
     (required #f)
     (value (required DAYS)
            (predicate ,(lambda (o) (number? (string->number o))))))
    (version
     "Print program version"
     (required #f)
     (single-char #\v)
     (value #f))
    (help
     "Prints this help"
     (required #f)
     (single-char #\h)
     (value #f))))

(define (banner)
  (printf "~a Version ~a (~a) -- ~a~%" program-name-string program-version commit-id program-description))

(define (do-add db-name db p e)
  (when (alist-ref e db equal?)
          (print "Error: An entry for '" e "' already exists.")
          (exit 1))
  (let ((user (prompt-for "Username" (random-username)))
        (password (new-password))
        (comment (prompt-for "Comment")))
    (encrypt-file db-name
                  (cons (list e user password comment (current-seconds)) db)
                  p)
    (print "Entry for " e " added.")))

(define (do-update db-name db p account)
  (cond ((alist-ref account db equal?) =>
         (lambda (e)
           (let ((user (prompt-for "User" (first e)))
                 (comment (prompt-for "Comment" (third e)))
                 (password (if (ask-yes-or-no "Change password?")
                               (new-password)
                               (second e))))
             (encrypt-file db-name
                           (alist-update account (list user password comment (current-seconds)) db equal?)
                           p)
              (print "Entry '" account "' has been updated."))))
        (else (fprintf (current-error-port) "Error: Entry for '~a' not found.~%" account)
              (exit 1))))

(define (do-delete db-name db p account)
  (cond ((alist-ref account db equal?) =>
         (lambda (e)
           (print-entries (list e) prefixes: (list account) show-header: #f)
           (cond ((ask-yes-or-no "Really delete account?")
                  (encrypt-file db-name  (alist-delete account db equal?) p)
                  (print "Entry '" account "' deleted."))
                 (else (print "Nothing done.")))))
        (else (fprintf (current-error-port) "Error: Entry for '~a' not found.~%" account)
              (exit 1))))

(define (do-list db account)
  (let* ((regex (string->irregex (if (string? account)
                                     (string-append ".*" account ".*")
                                     ".*")
                                 'i 'utf8))
         (accounts
          (sort (filter (lambda (k)
                          (irregex-match regex k))
                        (db-keys db))
                string<=)))
    (cond ((null? (db-keys db))
           (print "Error: No keys in password store")
           (exit 1))
          ((null? accounts)
           (print "Error: No entry for " account " found.")
           (exit 1))
          (else
           (print-entries (map
                           (lambda (account-name)
                             (alist-ref account-name db equal?))
                           accounts)
                          prefixes: accounts)))))

(define (do-password db e)
  (cond ((alist-ref e db equal?) =>
         (lambda (e)
           (display (second e))
           (when (terminal-port? (current-output-port))
             (newline))))
        (else 
         (fprintf (current-error-port) "Error: password for '~a' not found.~%" e)
         (exit 1))))

(define (do-init db-name content)
  (define (really-init)
    (print "I will ask you twice for the passphrase to encrypt the password store with.")
    (let ((passphrase1 (get-hashed-passphrase))
          (passphrase2 (get-hashed-passphrase)))
      (unless (equal? passphrase1 passphrase2)
              (print "Error: Passphrases do not match.")
              (exit 1))
      (encrypt-file db-name content passphrase1)
      (print "Password store " db-name " initialised.")))
  (cond ((and (check-access db-name)
              (ask-yes-or-no (sprintf "~a does exist, do you want to OVERWRITE ALL THE CONTENTS?" db-name)))
         (really-init))
        ((not (check-access db-name))
         (really-init))
        (else
         (print "Nothing done."))))

(define (do-change-passphrase db-name db old-passphrase)
  (print "I will ask you twice for the new passphrase.")
    (let ((passphrase1 (get-hashed-passphrase))
          (passphrase2 (get-hashed-passphrase)))
      (cond ((not (equal? passphrase1 passphrase2))
             (print "Error: Passphrases do not match.")
             (exit 1))
            ((equal? passphrase1 old-passphrase)
             (print "Error: Passphrase is the same as old passphrase")
             (exit 1))
            (else (encrypt-file db-name db passphrase1)
                  (print "Password store " db-name " reencrypted.")))))

(define (do-merge db-name db passphrase theirs)
  (define (merge-entries account mine theirs)
    (let ((show-password? (ask-yes-or-no "Show passwords?")))
      (print "Account " account)
      (let dialog-loop ()
        (print-entries (list mine theirs) prefixes: '("MINE" "THEIRS") show-password: show-password? show-headers: #f)
        (unless (or (equal? (second mine) (second theirs)) show-password?)
          (print "Password MISMATCH"))
        (let ((choice (ask-for-choice "Use which version?" "m" "t" "s" "?")))
          (case choice
            ((#\m) (print "Taking my version") mine)
            ((#\t) (print "Taking their version") theirs)
            ((#\s) (print "Skipping " account " keeping ours.") mine)
            ((#\?) (printf "m\ttake my version.~%t\ttake their version~%s\tskip the decision, same as \"m\"~%?\tthis help~%")
             (dialog-loop))
            (else (dialog-loop)))))))
  (print "Enter passphrase for db file " theirs)
  (let* ((passphrase-theirs (get-hashed-passphrase))
         (db-theirs (or (with-input-from-string
                         (or (decrypt-file theirs passphrase-theirs) "#f") read)
                        (begin (fprintf (current-error-port) "Error while decrypting ~a, wrong key?~%" theirs) (exit 1)))))
    (cond ((equal? db db-theirs)
           (print "Databases are the same."))
          (else
           (encrypt-file
            db-name
            (fold
             (lambda (entry db)
               (let ((account (car entry))
                     (theirs (cdr entry)))
                 (cond ((equal? theirs (alist-ref account db equal?)) db)
                       ((alist-ref account db equal?) =>
                        (lambda (ours)
                          (let ((new (merge-entries account ours theirs)))
                            (alist-update account new db equal?))))
                       (else
                        (print-entries (list theirs) prefixes: (list (sprintf "NEW ~a" account)) show-headers: #f)
                        (alist-cons account theirs db)))))
             db
             db-theirs)
            passphrase)))))

(define (do-age-check db days)
  (define (expires-in-seconds entry)
    (+ (fifth entry)
       (* 60 60 24 days)))
  (let* ((now (current-seconds))
         (old-passwords
          (filter
           (lambda (p)
             (< (expires-in-seconds p) now))
           db)))
    (if (pair? old-passwords)
        (begin
          (print "These passwords are older than " days " days.")
          (do-list old-passwords 'all))
        (print "Your passwords are younger than " days " days."))))

(define (main args)
  (set-buffering-mode! (current-output-port) #:none)
  (when (null? args)
        (banner) (print (usage options)) (exit 1))
  (let* ((opts
         (condition-case
          (getopt-long args options)
          (e (exn)
             (print "Error: "
                    ((condition-property-accessor 'exn 'arguments) e) " "
                    ((condition-property-accessor 'exn 'message) e))
             (banner)
             (print (usage options))
             (exit 1))))
         (db-name (or (alist-ref 'database-file opts)
                      (make-pathname (get-environment-variable "HOME") ".passdb")))
         (init (alist-ref 'init opts)))

    (unless (null? (alist-ref '@ opts equal?))
            (fprintf (current-error-port) "Warning: superfluous option given: ~a~%" (alist-ref '@ opts equal?)))
    (fprintf (current-error-port) "Using database file ~a~%" db-name)
    (unless (or init (check-access db-name))
            (print "Error database " db-name " does not exist or has wrong permissions.") (exit 1))
    (cond
     ((alist-ref 'help opts) (banner) (print (usage options)))
     ((alist-ref 'version opts) (banner))
     ((alist-ref 'import opts) => (lambda (f)
                                    (do-init db-name (with-input-from-file f read))))

     (init (do-init db-name '()))
     (else
      (let* ((passphrase (get-hashed-passphrase))
             (db (or (with-input-from-string
                         (or (decrypt-file db-name passphrase) "#f") read)
                     (begin (fprintf (current-error-port) "Error while decrypting ~a, wrong key?~%" db-name) (exit 1)))))
        (cond
         ((alist-ref 'change-passphrase opts)
          (do-change-passphrase db-name db passphrase))
         ((alist-ref 'add opts) => (lambda (e) (do-add db-name db passphrase e)))
         ((alist-ref 'list opts) => (lambda (e) (do-list db e)))
         ((alist-ref 'export opts) (pp db))
         ((alist-ref 'delete opts) => (lambda (e) (do-delete db-name db passphrase e)))
         ((alist-ref 'update opts) => (lambda (e) (do-update db-name db passphrase e)))
         ((alist-ref 'password opts) => (lambda (e) (do-password db e)))
         ((alist-ref 'merge opts) => (lambda (theirs) (do-merge db-name db passphrase theirs)))
         ((alist-ref 'check-age opts) => (lambda (days) (do-age-check db (string->number days))))
         (else (banner) (print "Error: Don't know what to do") (print (usage options)) (exit 1))))))
    (exit 0)))
)

(import pee (chicken process-context))
(main (cdr (argv)))
